# Tic-Tac-Toe
import random
def userChoice():
    print("Welcome to Tic-Tac-Toe!")
    print("Do you want to be X or O?")
    while True:
        userChoice=input().upper()
        computer=''
        if(len(userChoice)!=1):
            print("Please enter a single letter")
        elif(userChoice=='X'):
             computer='O'
             return userChoice,computer
        elif(userChoice=='O'):
             computer='X'
             return userChoice,computer
        else:
            print("Please enter a valid letter")
def firstMove():
    firstMovePC = random.randint(0,1)
    firstMovePC=0
   # print(firstMovePC)
    if (firstMovePC==1):
        print("The computer will go first.")
    else:
        print("You will go first.")
    return firstMovePC
def userPlay(boardCount):
    print("What is your next move? (1-%s)" %boardCount)
    userPlayed = int(input())
    while True:
        if userPlayed in range(1,boardCount+1):
            return userPlayed
        else:
            print("Please enter a valid move")
def drawBoardUser():
      boardSize = len(board)
      print("What is your next move? (1-%s)" %boardCount)
      drawBoard()
def drawBoard():
      i=boardSize - 1
      while(i>=0):
        for j in range(boardSize):
            print(board[i][j],end='')
            if (j == boardSize -1 ):
                print('')
            else:
                print('|',end='')
        if (i !=0 ):
            print('-+' * (boardSize-1) ,end='-\n')
        i-=1
def isOther(item,pcTurn):
    if (pcTurn):
        return item == userSelected
    else:
        return item == computerSelected  

def isSame(item,pcTurn):
    if (pcTurn):
        return item == computerSelected
    else:
        return item == userSelected   

def isEmpty(item):
    return item == ' '
def rowMatch(pcTurn):
        global rowWin,nearlyWin
        rowX,rowY=0,0
        for i in range(0,boardSize):
            itemPriority=0
            for b in range(0,boardSize):
                if (isOther(board[i][b],pcTurn)):
                    itemPriority+=1
            matchCount=0
            for j in range(0,boardSize):
                if (isSame(board[i][j],pcTurn)):
                    matchCount+=1
                elif(isEmpty(board[i][j])):
                    x,y=i,j
                    if (itemPriority != 0):
 #                       print("row x,y,itemPriority",x,y,itemPriority)
                        putIfEmpty(x,y,itemPriority)
            if (matchCount==boardSize):
                rowWin = True
                break
            elif (matchCount==boardSize-1 and itemPriority==0):
                nearlyWin=True
                rowX,rowY=x,y
        return rowX,rowY
def columnMatch(pcTurn):
        global columnWin,nearlyWin
        columnX,columnY=0,0
        for j in range(0,boardSize):
            itemPriority=0
            for b in range(0,boardSize):
                if (isOther(board[b][j],pcTurn)):
                    itemPriority+=1
            matchCount=0
            for i in range(0,boardSize):
                if (isSame(board[i][j],pcTurn)):
                    matchCount+=1
                elif(isEmpty(board[i][j])):
                      x,y=i,j
                      if (itemPriority != 0):
  #                        print("column x,y,itemPriority",x,y,itemPriority)
                          putIfEmpty(x,y,itemPriority)
            if (matchCount==boardSize):
                columnWin=True               
                break
            elif (matchCount==boardSize-1 and itemPriority==0):
                 nearlyWin=True
                 columnX,columnY=x,y
        return columnX,columnY
def crossMatch(pcTurn):
        columnX,columnY=0,0
        global crossWin,nearlyWin
        matchCount=0
        itemPriority=0
        for b in range(0,boardSize):
              if (isOther(board[b][b],pcTurn)):
                  itemPriority+=1
        for i in range(0,boardSize):              
              if (isSame(board[i][i],pcTurn)):                  
                  matchCount+=1
              elif(isEmpty(board[i][i])):
                    x,y=i,i
                    if (itemPriority != 0):
  #                      print("cross x,y,itemPriority",x,y,itemPriority)
                        putIfEmpty(x,y,itemPriority)
        if (matchCount==boardSize):
            crossWin=True                                     
        elif (matchCount==boardSize-1 and itemPriority==0):
            nearlyWin=True
            columnX,columnY=x,y
        return columnX,columnY
def  reverseCrossMatch(pcTurn):
        columnX,columnY=0,0
        global reverseCrossWin,nearlyWin
        matchCount=0
        itemPriority=0
        for b in range(0,boardSize):
              if (isOther(board[b][boardSize-1-b],pcTurn)):
                  itemPriority+=1
        for i in range(0,boardSize):
              if (isSame(board[i][boardSize-1-i],pcTurn)):
                  matchCount+=1
              elif(isEmpty(board[i][boardSize-1-i])):
                    x,y=i,boardSize-1-i
                    if (itemPriority != 0):
   #                     print("reversecross x,y,itemPriority",x,y,itemPriority)
                        putIfEmpty(x,y,itemPriority)   
        if (matchCount==boardSize):
            reverseCrossWin=True                                     
        elif (matchCount==boardSize-1 and itemPriority==0):
            nearlyWin=True
            columnX,columnY=x,y
        return columnX,columnY
def willPCWin(funcPCWin):
        global pcWin,nearlyWin
        priority=10
        matchX,matchY=funcPCWin(True)
        #print("funcnearlyPc!!!:",matchX,matchY, nearlyWin)
        if (rowWin or columnWin or crossWin or reverseCrossWin):
            pcWin= True
        elif (nearlyWin):
            putIfEmpty(matchX,matchY,priority)
            pcWin= False
            nearlyWin=False
        return
def willUserWin(funcUserWin):
        global userWin,nearlyWin
        priority=20
        matchX,matchY=funcUserWin(False)
       # print("funcnearlyUser!!!:",matchX,matchY, nearlyWin)
        if (rowWin or columnWin or crossWin or reverseCrossWin):
            userWin= True
        elif (nearlyWin):
            putIfEmpty(matchX,matchY,priority)
            userWin= False
            nearlyWin=False
        return
def userMovePut():
    global userWin
    userNumber =userPlay(boardCount) -1
    x=userNumber // boardSize
    y=userNumber % boardSize
    moveIfEmpty(x,y,userSelected)
    willUserWin(rowMatch)
    willUserWin(columnMatch)
    willUserWin(crossMatch)
    willUserWin(reverseCrossMatch)
    return
def moveIfEmpty(x,y,selection):
        if (' ' == board[x][y] ):
            board[x][y] = selection
        else:
            print("Not Empty Board!!!:",x, y)
        return
def putIfEmpty(x,y,priority):
        global pri
        global thisArray        
        if(priority < 10):
           if (x==center and y==center):
              priority =priority + 30
           elif ((x==cornerMin and y==cornerMin) or
                 (x==cornerMin and y==cornerMax) or
                 (x==cornerMax and y==cornerMin) or
                 (x==cornerMax and y==cornerMax)):
              priority =priority + 40
           else:
              priority=priority + 50 
        if (pri!=priority):
            thisArray.clear()

        if (' ' == board[x][y] ):
            thisArray.append(toHash(x,y))            
            thisDict[priority] = thisArray.copy()       
        else:
            print("Used Dict Slot!!!:",priority,x, y)
         
        pri =  priority   
        return

def toHash(x,y):
    return (x * boardSize) + y + 1    

def toCoor(hashNumber):
    hashNumber-=1
    x=hashNumber // boardSize 
    y=hashNumber % boardSize
    return x,y        

def centerFill():
        priority = 30
        putIfEmpty(center,center,priority)
        return
def cornerFill():
        priority =  40
        randomCorner=random.randint(0,cornerMax)
        if(randomCorner==0):
            putIfEmpty(cornerMin,cornerMin,priority)
        elif(randomCorner==1):
            putIfEmpty(cornerMin,cornerMax,priority)
        elif(randomCorner==2):
            putIfEmpty(cornerMax,cornerMin,priority)
        elif(randomCorner==3):
            putIfEmpty(cornerMax,cornerMax,priority)
        return
def randomXY():
            randomSide=random.randint(0,boardCount-1)
            x=randomSide // boardSize
            y=randomSide % boardSize
            return x,y
def sideFill():
        priority =    50
        x,y=randomXY()
  #      print("sideFill:",x, y)
        while True:
            if   (x== center and y==center):
                x,y=randomXY()
            elif (x== cornerMin and y==cornerMin):
                x,y=randomXY()
            elif (x== cornerMin and y==cornerMax):
                x,y=randomXY()
            elif (x== cornerMax and y==cornerMin):
                x,y=randomXY()
            elif (x== cornerMax and y==cornerMax):
                x,y=randomXY()
            else:
                putIfEmpty(x,y,priority)
                return
def setPcMoveFromDict():
       thisLength=len(thisDict.get(min(thisDict.keys())))
       xCoor,yCoor = toCoor(thisDict.get(min(thisDict.keys()))[random.randint(0,thisLength-1)])
       moveIfEmpty(xCoor,yCoor,computerSelected)
       thisDict.pop(min(thisDict.keys()))
       return
def pcMovePut():

        willPCWin(rowMatch)
        willPCWin(columnMatch)
        willPCWin(crossMatch)
        willPCWin(reverseCrossMatch)
        willUserWin(rowMatch)
        willUserWin(columnMatch)
        willUserWin(crossMatch)
        willUserWin(reverseCrossMatch)
        if (pri==0):
          centerFill()
          #cornerFill()
          #sideFill()
      #  for x, y in thisDict.items():
      #      print("Pcdict:",x, y)
        setPcMoveFromDict()
        return
def userMove():
        global userMoved
        userMovePut()
     #   drawBoardUser()
      #  for x, y in thisDict.items():
      #        print("Userdict:",x, y)
        userMoved= True
def pcMove():
        global userMoved
        pcMovePut()
        drawBoard()
        userMoved= False

print("What is your board size? (3-5)")
boardSize = int(input())
boardCount = boardSize * boardSize
pcWin,userWin,rowWin,columnWin,crossWin,reverseCrossWin, nearlyWin = False,False,False,False,False,False,False

board =[ [ " " for i in range(boardSize) ] for j in range(boardSize) ]
userSelected,computerSelected=userChoice()
firstMovePC = firstMove()
thisDict={}
thisArray=[]
cornerMin= 0
cornerMax= boardSize-1
center= int((boardSize-1)/2)
pri=0
drawBoardUser()
userMoved = False
if (firstMovePC==0): # user play
       userMove ()
else: #pc play
       pcMove ()
while not (pcWin or userWin):
    if (userMoved):
       pcMove()
    else:
       userMove()

